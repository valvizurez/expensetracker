//
//  ViewController.swift
//  expenseTracker
//
//  Created by Victoria Alvizurez on 10/4/17.
//  Copyright © 2017 Victoria Alvizurez. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {
    @IBOutlet var tableView: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return tasks.count
    }
    var valueToPass = ""
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // get cell label
        let indexPath = tableView.indexPathForSelectedRow;
        let currentCell = tableView.cellForRow(at: indexPath!) as UITableViewCell!;
        valueToPass = (currentCell?.textLabel?.text)!
       self.performSegue(withIdentifier: "moveCell", sender: self)
   
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "moveCell") {
            // initialize new view controller and cast it as your view controller
            let viewController = segue.destination as! expenseViewController
            // your new view controller should have property that will store passed value
            viewController.passedValue = valueToPass
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for:indexPath) as UITableViewCell
        let task = tasks[indexPath.row]
        
        if let myName = task.name {
            cell.textLabel?.text = myName
        }
       return cell
    }
    
    override func viewWillAppear(_ animated: Bool) { getData(); tableView.reloadData() }
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var tasks: [Payment] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let task = Payment(context: context)
       // task.name = "worked"
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        getData()
        print(tasks)
        
        
    }

    func getData(){
        do{
            tasks = try context.fetch(Payment.fetchRequest())
        }
        catch{
            print("Fetching Failed")
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

