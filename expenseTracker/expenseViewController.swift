//
//  expenseViewController.swift
//  expenseTracker
//
//  Created by Victoria Alvizurez on 10/6/17.
//  Copyright © 2017 Victoria Alvizurez. All rights reserved.
//

import UIKit
import CoreData

class expenseViewController: UIViewController {
    
    var passedValue = ""
    var cellName = ""
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var tasks: [Payment] = []

    @IBOutlet var expenseName: UITextView!
    @IBOutlet var date: UITextView!
    @IBOutlet var location: UITextView!
    @IBOutlet var cost: UITextView!
    @IBOutlet var notes: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cellName = passedValue
        // Do any additional setup after loading the view.
    getInfo()
        
    }
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Payment> {
        return NSFetchRequest<Payment>(entityName: "Payment")
    }
    func getInfo(){
        do{
            let request : NSFetchRequest <Payment> = Payment.fetchRequest()
            request.predicate = NSPredicate(format: "name == %@", cellName)
            let fetchedResults = try context.fetch(request)
            
            if let results = fetchedResults.first{
                expenseName.text = results.name
                date.text = results.date
                location.text = results.location
                cost.text = results.cost
                notes.text = results.notes                
            }
          
        }
        catch {
            print("fetch task failed",error)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    
    }
    
    func getData(){
        do{
            tasks = try context.fetch(Payment.fetchRequest())
        }
        catch{
            print("Fetching Failed")
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
