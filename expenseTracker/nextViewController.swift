//
//  nextViewController.swift
//  expenseTracker
//
//  Created by Victoria Alvizurez on 10/5/17.
//  Copyright © 2017 Victoria Alvizurez. All rights reserved.
//

import UIKit

class nextViewController: UIViewController {
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var tasks: [Payment] = []
    
    @IBOutlet var expenseName: UITextField!
    @IBOutlet var date: UITextField!
    @IBOutlet var location: UITextField!
    @IBOutlet var cost: UITextField!
    @IBOutlet var notes: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let saveBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.save, target: self, action: #selector(buttonTapped(_:)))
        
        let deleteBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.trash, target: self, action: #selector(buttonTapped(_:)))
        
        self.navigationItem.rightBarButtonItems = [saveBtn]
      //  self.navigationItem.leftBarButtonItems = [deleteBtn]
    }
    @objc func buttonTapped(_ sender : UIButton)
    {
        let tasks = Payment(context: context)
        tasks.name = expenseName.text!
        tasks.date = date.text
        tasks.location = location.text
        tasks.cost = cost.text
        tasks.notes = notes.text
        
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
         let _ = navigationController?.popViewController(animated: true)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        expenseName.resignFirstResponder()
        date.resignFirstResponder()
        location.resignFirstResponder()
        cost.resignFirstResponder()
         notes.resignFirstResponder()
        return true
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
